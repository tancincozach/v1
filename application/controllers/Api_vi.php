<?php
/*use Restserver\Libraries\REST_Controller;*/
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
/*require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';*/

class Api_vi extends CI_Controller{
//class Api extends CI_Controller{

public function __construct() {




		parent::__construct();
    	$this->load->helper('url');
    	$this->load->library('session');    	
    	$this->load->model('Ordersmodel','orders'); 


    	

	}


public function index(){

	///echo APPPATH.'libraries/REST_Controller.php';
	
		$result_orders = $this->orders->get_results();


		$this->load->view('index',array('data'=>$result_orders));
}



public function place_orders_post(){
		
		/**
		* Passes POST variables for segregation and insertion to the database
		* @return {Array|Object} return_array returns for insertion to database.
		*/
		
		$result_array = array();
	

		$posted_array = $this->input->post();



		try {

			$result_array = $this->process_and_segregate_data_orders($posted_array);	

			if(array_key_exists('error', $result_array ))
				throw new Exception($result_array['error']);
			
	    	http_response_code(201);

	    	
		} catch (Exception $e) {


			$result_array['error'] = $e->getMessage();

			
		}


	    //$this->response($result_array,201);



		echo json_encode($result_array);

	}

	public function process_and_segregate_data_orders( $posted_values ){
	
	  /**
	   * Identify posted data values if array or string value, segregate it and inserts/add  to the database.
	   * @param  POST variables
	   * @return {Array|Object} return_array returns the inserted info {id|stops|drivingDistancesInMeters|fare}
	   */
	  

		$return_array = array();

		$json_return = array('');
		

		$total_distance = 0;

		try {

			if(empty($posted_values) || count($posted_values)==0) 

				throw new Exception("Empty POST DATA");


			$avoid_key = array('distance_per_stop');


			foreach ($posted_values as $key => $val) {


				if(!in_array($key, $avoid_key)){

					if(is_array($val)){

						$return_array[$key] = json_encode($val);
					}		

				}else{		


					$dis_array = array();

					switch ($key) {

							case 'distance_per_stop':

								foreach ($posted_values['distance_per_stop'] as $dis_val) {
									
									$total_distance += (int)$dis_val['distance'];

									$dis_array[] = (int)$dis_val['distance'];

								}		
								
								break;
							
						}	
				}

				$return_array['drivingDistancesInMeters'] = json_encode($dis_array);
			}

			$fare_result  = $this->calculate_fare($total_distance);

			if(array_key_exists('error', $fare_result))

				throw new Exception( $fare_result['error']);						
						

		    $return_array['fare']  = json_encode($fare_result);
		    $return_array['status']  = 'ASSIGNING';


		    $id = $this->orders->add($return_array);

		    unset($return_array);

		    $return_array = $this->orders->get_results(
		    										array(
		    											'where'=>array('order_id'=>$id),
		    											'select'=>'order_id,drivingDistancesInMeters,fare'
		    											)		    										
		    									);



			
		} catch (Exception $e) {


			$return_array['error'] = $e->getMessage();
		}



		return $return_array;

	}

	public function calculate_fare($distance){

	  /**
	   * Calculates the rate per distance.
	   * @param  (int) total distance
	   * @return {Array|Object} return_array returns the calculated rate and error message if there is an error {fare|error}
	   */

		$time_now   = strtotime(date('H:i:s'));

		$start_time = strtotime('21:00:00');

		$end_time   = strtotime('05:00:00');

		$fare = 20;

		$return_array = array();


		try {

			if(empty($distance) || $distance=='') throw new Exception("Empty Distance Parameter");
			
					
					$distance = $distance - 2000;
					
					if(($time_now>=$start_time ) AND ($time_now<=$end_time)){

						// initial fare is  30 HKD if  the time is under the range of 9pm - 5 am.
						
						$fare = 30;

					}
					
					if($distance > 0){

							// from 9pm - 5am 
						
							if(($time_now>=$start_time ) AND ($time_now<=$end_time)){

								// each 200 meter costs HKD 8						
								$fare +=(($distance/200)*8);
								
							}else{
								// each 200 meter costs HKD 5

								$fare +=(($distance/200)*5);
							}

					}	

		    $return_array['fare']  = array('amount'=>$fare,'currency'=>'HKD');



			
		} catch (Exception $e) {

				$return_array['error'] =  $e->getMessage();
			
		}		




			return $return_array;			

	}


	
	public function process_order(){


	  /**
	   * Identify order (take,complete and cancel) and update the order status
	   * @return {Array|Object} return_array returns the calculated rate and error message if there is an error {fare|error}
	   */
	  


		$return_array = array();

	    $id =  $this->uri->segment(2);

	    $process =  $this->uri->segment(3);

		try {


			if(empty($id) || $id==''){
				
				http_response_code(422);

				throw new Exception("Empty Order ID",422);
			}

			if(empty($process) || $process==''){
				
				http_response_code(422);

				throw new Exception("Empty Process Type",422);
			}



			$row = $this->orders->get_row($id);


			if(empty($row) || count($row)==0){

				http_response_code(404);
				throw new Exception("Order does not exist",404);

			}


			$set = array();

					switch ($process) {
						
						case 'take':	

							$set['status'] = 'ONGOING';
							$set['ongoingAt'] = date('Y-m-d H:i:s');
		
							break;
						
						case 'complete':

							$set['status'] = 'COMPLETED';
							$set['completedAt'] = date('Y-m-d H:i:s');
							break;
						
						case 'cancel':

							$set['status'] = 'CANCELLED';
							$set['cancelledAt'] = date('Y-m-d H:i:s');
			
							break;

						default:
							
							break;
					}

				$this->orders->update($id,$set);

				$row = $this->orders->get_row($id);

				$filter['where'] = array('order_id'=>$id);
				

				switch ($process) {
						
						case 'take':	

							$filter['select'] = 'order_id as id,status,ongoingAt';
		
							break;
							
						case 'complete':

							$filter['select'] = 'order_id as id,status,completedAt';

							break;
						
						case 'cancel':

							$filter['select'] = 'order_id as id,status,cancelledAt';
							break;

						default:
							
							break;
					}
				
					$return_array = $this->orders->get_results( $filter);

					echo json_encode($return_array);

			
		} catch (Exception $e) {

			echo $e->getMessage();
			
		}

	}

}
