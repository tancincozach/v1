<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ordersmodel extends CI_Model{

  var $table = 'orders';
  var $id = 'order_id';


  function add( $set )
  {


    try{




      if($this->db->insert($this->table, $set))
      {
        $id = $this->db->insert_id();
        

        return $id;
      }
      else
      {
         return 0;
      }
    }
    catch(Exception $error)
      { 
        return  array('error'=>$error->getMessage());
      } 
  }


  function update( $id, $set )
  {
    
    if( $id <= 0 OR $id=='' ) return 0;



      try {



      $this->db->where($this->id, $id);
  

      if($this->db->update($this->table, $set))
      {
        


        return $id;
      }
      else
      {
         return 0;
      }

    } catch (Exception $e) {

      return array('error'=>$e->getMessage());
    } 
    
  }

   function delete($parameter)
     {



       try {

            if( $parameter['id'] <= 0 OR $parameter['id']=='' ) throw new  Exception("Error : Empty ID for delete");
                  
                $set  = $this->get_row($parameter['id']);
              
              if($this->db->where($this->id, $parameter['id'])
                     ->delete($this->table))
              {
          return true;
              }
              else{
          return false;
              }

        } catch (Exception $e) {
            return array('error'=>$e->getMessage());
        }
    }

  function get_row( $id ){
    try {

        if( $id <= 0 OR $id=='' ) throw new  Exception("Error : Empty ID for fetching row");

        $this->db->where($this->id, $id);

        $query = $this->db->get($this->table);

        return $query->row();

        
    } catch (Exception $e) {
        
    }
   }   


    function get_results($params=array(), $result='result') {

          //where clause
      try {

          if(isset($params['select'])){
              $this->db->select($params['select']);
          } 

          if(isset($params['where'])){
              $this->db->where($params['where']);
          }       

          if(isset($params['where_str']) && $params['where_str']!='' ){
              $this->db->where($params['where_str'], null, false);             
          }
              $query = $this->db->get($this->table);

              
              $result = $query->result();

        

          return $result;
      } catch (Exception $e) {
          return array('error'=>$e->getMessage());   
      }
        
    }

}