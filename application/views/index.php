<html>
	
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</head>
<style type="text/css"></style>

	 <body class="bg-light">

    <div class="container">
      

      <div class="row">

        <div class="col-md-12 ">
          <h4 class="mb-3">API Direction Test Interface</h4> 

            <div class="row">
              <div class="col-md-3 mb-3">
                <label for="start_direction">Start</label>
                <input type="text" class="form-control" id="start_direction" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="end_direction">End</label>
                <input type="text" class="form-control" id="end_direction" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div> 
              <div class="col-md-3 mb-3">
                <label for="end_direction">Driver</label>
                <input type="text" class="form-control" id="end_direction" placeholder="" value="" required>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
              <div class="col-md-3 mb-3" style="padding-top:30px">
              	
              <div class="btn-group" role="group" aria-label="Basic example">					  
					  <button type="button" class="btn btn-info" id="submit">Submit</button>					  
				</div>
              </div>
            </div>

          
            		<div class="col-md-12 col-lg-12 " id="map" style="height:400px;width: 100%">
            			
            		</div>


     		<input type="hidden" name="current_loc" value=""/>
            <hr class="mb-4">
            

        </div>

        <div class="row">
        		
        		<div class="col-md-12">

        			<table class="table">

        			   <thead>
	        				<tr>
	        					<th>Order Id</th>	
	        					<th>Options</th>
	     
	        				</tr>	        				
        			   	
        			   </thead>

        			   	<tbody>
        			   	<?php 
        			   		if(count($data) > 0):
        			   			foreach($data as $orders):?>
        			   		<tr>
        			   			<td><?php echo $orders->order_id;?></td>
        			   			<td>
        			   			<button class="btn btn-sm btn-primary" onclick="order.take_or_cancel(<?php echo $orders->order_id?>,'take');">Take</button>&nbsp;
        			   			<button class="btn btn-sm btn-warning" onclick="order.take_or_cancel(<?php echo $orders->order_id?>,'cancel');">Cancel</button></td>
        			   		</tr>
			   			<?php
			   					endforeach;
			   				 endif;
			   			?>
        			   	</tbody>
        				
        			</table>
        			
        		</div>

        </div>
      </div>



      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2017-2018 Company Name</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="#">Privacy</a></li>
          <li class="list-inline-item"><a href="#">Terms</a></li>
          <li class="list-inline-item"><a href="#">Support</a></li>
        </ul>
      </footer>
    </div>
    <script>


   	 var GOOGLE_MAP_KEY = 'AIzaSyDqkJAFTcc9jrYbq15onxb4pXYB_LK6hjk',
   	 	directions = {'start_direction':'','end_direction':''};
   	 

   	 var order = {

   	 		take_or_cancel:function( id_val,process_val){

   	 			switch(process_val){

   	 				case 'take':
   	 				case 'cancel':

	    			$.ajax({
   	 	    			url:'orders/'+id_val+'/'+process_val,
   	 	    			type:'PUT',
   	 	    			success:function(result){

   	 	    				console.log(result);
   	 	    			},
   	 	    			dataType:'JSON'
   	 	    	});

   	 			}

   	 		},


   	 	    place_order:function( parameters){


   	 	    	$.ajax({
   	 	    			url:'orders',
   	 	    			type:'POST',
   	 	    			data:parameters,
   	 	    			success:function(result){

   	 	    				console.log(result);
   	 	    			},
   	 	    			dataType:'JSON'
   	 	    	});

   	 	    }
   	 }

     var  google_map_object = {



	     	calculate_and_display_route:function(type,ac_obj,directionsService, directionsDisplay){

	  		var place = ac_obj.getPlace() , dir_details = {};
	  		 

				dir_details = {name:place.formatted_address,lat: place.geometry.location.lat(),lng:place.geometry.location.lng()};


		  		 if(type=='start'){

		  		 	directions.start_direction = dir_details;

		  		 }else{
		  		 	
		  		 	directions.end_direction = dir_details;
		  		 }


		  		// console.log(directions);


		  		 if(directions.start_direction!='' && directions.end_direction!=''){

					            directionsService.route({
						          origin: directions.start_direction,
						          destination: directions.end_direction,
						          travelMode: 'DRIVING'
						        }, function(response, status) {


						          if (status === 'OK') {
									
						         	if(!$.isEmptyObject(response)){



						         		 var route = response.routes[0] , 
							         		 steps = route.legs[0].steps,
							         		 temp_lat_lng_array = [],
							         		 temp_distance_array = [],
							         		 post_object = {},
							         		 total_distance = 0,
							         		 ctr_per_stop = 0;

							         		
						         			$.each(steps,function(i,step_val){

						         				var ctr=0;

						         				console.log(step_val);
						         				$.each(step_val.lat_lngs,function(i2,coordinates){

														temp_lat_lng_array[ctr]= {lat:coordinates.lat(),lng:coordinates.lng};

						         					ctr++;
						         				});

						         				
			         							total_distance+=parseInt(step_val.distance.value);

			         							//temp_distance_array.push(step_val.distance.value);
			         							temp_distance_array[ctr_per_stop] = {distance:step_val.distance.value};
						         				ctr_per_stop++;	
						         			});
			         							
						         			post_object.distance_per_stop = temp_distance_array;
						         			post_object.stops = temp_lat_lng_array;
						         		 	//post_object.total_distance = total_distance;

								             $('#submit').show();

								              $('#submit').on('click',function(event){

								              				order.place_order(post_object);
								              				event.preventDefault()	;

								              });						         		 						         		 
						         	}


						          	//console.log(response.routes[0]);						          	
						            directionsDisplay.setDirections(response);
						              	


						          } else {
						            window.alert('Directions request failed due to ' + status);
						          }
						        });

		  		 }

	  		

	     	},

	     	get_address:function (latitude,longitude){

				  $.ajax('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&key='+
				          GOOGLE_MAP_KEY)
				  .then(
				    function success (response) {
				      console.log('User\'s Address Data is ', response)
				    },
				    function fail (status) {
				      console.log('Request failed.  Returned status of',
				                  status)
				    }
				   )
	     	},

	     	
	     	 ip_look_up:function () {

				  $.ajax('http://ip-api.com/json')
				  .then(
				      function success(response) {
				          console.log('User\'s Location Data is ', response);
				          console.log('User\'s Country', response.country);
				          $('input[name=current_loc]').val(JSON.stringify(response));
				      },

				      function fail(data, status) {
				          console.log('Request failed.  Returned status of',
				                      status);
				      }
				  );
			},



     		init_map:function(){

     			
		  	    var  ac_start,ac_end;

		         directionsService = new google.maps.DirectionsService;
		         directionsDisplay = new google.maps.DirectionsRenderer;


		        var map = new google.maps.Map(document.getElementById('map'), {
		          zoom: 15,
		          center: {lat: 41.85, lng: -87.65}
		        });
   
				ac_start = new google.maps.places.Autocomplete( (document.getElementById('start_direction')), {types: ['geocode']});


				ac_end = new google.maps.places.Autocomplete((document.getElementById('end_direction')),{types: ['geocode']});



 			 google.maps.event.addListener(ac_start, 'place_changed', function() {

                		google_map_object.calculate_and_display_route('start',ac_start,directionsService, directionsDisplay);
            });

 			 google.maps.event.addListener(ac_end, 'place_changed', function() {
                		google_map_object.calculate_and_display_route('end',ac_end,directionsService, directionsDisplay);
            });



		        infoWindow = new google.maps.InfoWindow;

		        // Try HTML5 geolocation.
		        if (navigator.geolocation) {
		          navigator.geolocation.getCurrentPosition(function(position) {

		            var pos = {
		              lat: position.coords.latitude,
		              lng: position.coords.longitude
		            };

		            google_map_object.get_address(pos.lat,pos.lng);

		            map.setCenter(pos);
		          }, function() {
		            handleLocationError(true, infoWindow, map.getCenter());
		          });
		        } else {
		          // Browser doesn't support Geolocation
		          	 $.ajax('http://ip-api.com/json')
							  .then(
							      function success(response) {


							          console.log('User\'s Location Data is ', response);
							          console.log('User\'s Country', response.country);

							                var directionsService = new google.maps.DirectionsService;
									        var directionsDisplay = new google.maps.DirectionsRenderer;
									        var map = new google.maps.Map(document.getElementById('map'), {
									          zoom: 10,
									          center: {lat: response.lat, lng: response.lon}
									        });

											map.setCenter(pos);

									        directionsDisplay.setMap(map);

									        document.getElementById('submit').addEventListener('click', function() {
									          google_map_object.calculate_and_display_route(directionsService, directionsDisplay);
									        });
							      }
							      ,

							      function fail(data, status) {
							          console.log('Request failed.  Returned status of',
							                      status);
							      }
							  );

		          handleLocationError(false, infoWindow, map.getCenter());
		        }

		        directionsDisplay.setMap(map);

		
		        $('#submit').hide();
		

     		}	
     }



    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqkJAFTcc9jrYbq15onxb4pXYB_LK6hjk&libraries=places&callback=google_map_object.init_map">
    </script>



  </body>

</html>